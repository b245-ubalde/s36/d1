const express = require("express");
const router = express.Router();

const taskController = require("../Controllers/taskControllers.js");

/*
	Routes to be inputted here!
*/



// Route for getAll
router.get("/", taskController.getAll);

// Route for createTask
router.post("/addTask", taskController.createTask);

// Route for deleteTask
router.delete("/deleteTask/:id", taskController.deleteTask);

// Route for getting a specific task
router.get("/:id", taskController.getSpecificTask);

// Route for changing the status of a specific task to "complete"
router.put("/:id/complete", taskController.changeStatusToComplete);


module.exports = router;