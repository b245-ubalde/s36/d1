const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./Routes/taskRoute.js");

const app = express();
const port = 3001;


	// MongoDB connection
	mongoose.connect("mongodb+srv://admin:admin@batch245-ubalde.4valdm5.mongodb.net/s35-discussion?retryWrites=true&w=majority", 
			{
				//Allows us to avoid any current and future errors while connecting to MONGODB
				useNewUrlParser: true,
				useUnifiedTopology: true

		})


	// Check connection

	let db = mongoose.connection;

	// error catcher
	db.on("error", console.error.bind(console, "Connection Error!"))

	// confirmation of the connection
	db.once("open",() => console.log("We are now connected to the cloud! Y E Y!"))


// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// routing
app.use("/tasks", taskRoute)



app.listen(port, () => console.log(`Server is running at port ${port}`));



/*
	Separation of Concerns

	- Model should be connected to the Controller.
		taskController.js needs objects declared in Model(task.js) therefore, require Model in taskController

	- Controller should be conencted to the Routes.
		Routes(taskRoute.js) needs functions declared in Controller(taskController.js) therefore, require Controller in Routes

	- Routes should be connected to the server/application
		Server(server.js) needs routes (get, post, put, delete methods) in Route(taskRoute.js) therefore, Route in server

	model >> controller >> routes >> server
*/