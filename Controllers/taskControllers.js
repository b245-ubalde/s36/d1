const Task = require("../Models/task.js");


/*
	Controllers and Functions
*/


// Controller/Function to get all the task on our database
module.exports.getAll = (request, response) => {

	Task.find({})
	// to capture the result of the find method
	.then(result => {
		return response.send(result)
	})
	// the catch method captures the error when the find method is executed
	.catch(error => {
		return response.send(error);
	})

}


// Add Task on our Database
// .then - for successful
// .catch - for errors
module.exports.createTask = (request, response) => {
	const input = request.body;
				// criteria
	Task.findOne({name: input.name})
	.then(result => {
		if(result !== null){
			return response.send("The task is already existing!");
		}
		else{
			let newTask = new Task({
				name: input.name
			});

			newTask.save().then(save => {
				return response.send("The task is successfully added!");
			}).catch(error => {
				return response.send(error);
			})
		}
	})
	.catch(errors => {
		return response.send(error);
	})
}


// Controller the will delete the document that contains the given Object.
module.exports.deleteTask = (request, response) => {
	let idToBeDeleted = request.params.id;

	// findByIdAndRemove - to find the document that contains the id and then delete the document
	Task.findByIdAndRemove(idToBeDeleted)
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(errors);
	})
}


// Controller for getting a specific task
module.exports.getSpecificTask = (request, response) => {
	let idtoBeSearched = request.params.id;

	Task.findById(idtoBeSearched)
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(errors);
	})
}


// Controller for changing a specific task's status to "complete"
module.exports.changeStatusToComplete = (request, response) => {
	let idtoBeStatusChanged = request.params.id;

	// "new: true" sets the document to return the document after update was applied
	Task.findByIdAndUpdate(idtoBeStatusChanged, {status: "completed"}, {new: true})
	.then(result => {
		return response.send(result);
	})
}